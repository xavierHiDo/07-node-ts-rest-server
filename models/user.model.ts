import { DataTypes } from 'sequelize';
import db_connection from "../database/connection";

const User = db_connection.define('User', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  state: {
    type: DataTypes.BOOLEAN,
    allowNull: true
  },
});

export default User;